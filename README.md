# tenacityaudio.org

This is the source code for [tenacityaudio.org](https://tenacityaudio.org). Also
the home of https://tenacityteam.codeberg.page/tenacityaudio.org for staging
changes to the main website.

The website's source code is licensed under the [MIT License](LICENSE).

# Branches

* `pages` - Default branch. Deploys changes to
   https://tenacityteam.codeberg.page/tenacityaudio.org before pushing to the
   main site. For staging changes.

* `main` - Source code of the site that is hosted at https://tenacityaudio.org,
  the 'main' website.

