<!DOCTYPE html>

<html lang="en" data-content_root="../">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Installing Tenacity and Installation Notes &#8212; Tenacity 1.3e1 documentation</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css?v=f63d8bfa" />
    <link rel="stylesheet" type="text/css" href="../_static/haiku.css?v=fce32b03" />
    <script src="../_static/documentation_options.js?v=d95ff332"></script>
    <script src="../_static/doctools.js?v=9bcbadda"></script>
    <script src="../_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Basics" href="Basics.html" />
    <link rel="prev" title="Introduction" href="Introduction_and_Motivation.html" /> 
  </head><body>
      <div class="header" role="banner">
        <a href="../index.html">
          <img class="logo" src="../_static/tenacity-logo-light-200.png" alt="Logo of Tenacity"/>
        </a>
      </div>
      <div class="topnav" role="navigation" aria-label="Top">
      
        <p>
        «&#160;&#160;<a href="Introduction_and_Motivation.html">Introduction</a>
        &#160;&#160;::&#160;&#160;
        <a class="uplink" href="../index.html">Contents</a>
        &#160;&#160;::&#160;&#160;
        <a href="Basics.html">Basics</a>&#160;&#160;»
        </p>

      </div>
      <div class="content" role="main">
        
        
  <section id="installing-tenacity-and-installation-notes">
<h1>Installing Tenacity and Installation Notes<a class="headerlink" href="#installing-tenacity-and-installation-notes" title="Link to this heading">¶</a></h1>
<p>At the moment, Tenacity only provides binaries for Windows (32 and 64 bit) and
Linux (64 bit only). Unofficial packages also exist for Tenacity, such as the
<a class="reference external" href="https://archlinux.org/packages/extra/x86_64/tenacity/">Arch Linux tenacity package</a>,
the <a class="reference external" href="https://aur.archlinux.org/packages/tenacity-git%3E">AUR nightly package</a> and
the <a class="reference external" href="https://community.chocolatey.org/packages/tenacity/1.3.20230123-beta">Chocolatey package for Windows</a>.
Additionally, Tenacity supports other platforms like Haiku R1.</p>
<p>Officially, Windows installers and ZIP archives for both 32 and 64 bit versions
of Windows are provided. For Linux, AppImages are available for some versions
of Tenacity. Starting in 1.3 beta 3, Tenacity is now distributed as a Flatpak
on Linux.</p>
<p>If you are upgrading from a previous version, please read the
<a class="reference internal" href="#migration-notes">Migration Notes</a> for more information and advisories on
upgrading from previous versions.</p>
<section id="system-requirements">
<h2>System Requirements<a class="headerlink" href="#system-requirements" title="Link to this heading">¶</a></h2>
<p>Tenacity is neither a heavyweight nor lightweight application. Nevertheless,
your system should have at least some decent specs in order to run Tenacity.</p>
<section id="hardware">
<h3>Hardware<a class="headerlink" href="#hardware" title="Link to this heading">¶</a></h3>
<p>These hardware requirements are somewhat the same across all platforms</p>
<ul class="simple">
<li><p><strong>CPU</strong>: Your CPU should be equiped with SSE2. All modern 64-bit CPUs include
this feature. Do not worry about this on ARM.</p></li>
<li><p><strong>RAM</strong>: At least 2 GB available RAM for modest usage. Larger projects require
more RAM.</p></li>
<li><p><strong>Storage</strong>: At least 10-15 GB for modest usage, more for larger projects.</p></li>
<li><p><strong>Display</strong>: 1280x720 is recommended for comfortable usage.</p></li>
</ul>
<div class="admonition warning">
<p class="admonition-title">Warning</p>
<p>While we try our best to fix bugs in Tenacity, there are some that
inevitably exist and might take us a while to fix:</p>
<ul class="simple">
<li><p>Tenacity’s track area is <em>not</em> hardware accelerated and might consume an
undesirable amount of your CPU’s resources. We are experimenting with a
new track panel implementation and hope to add graphics acceleration to
the track area some time in the future.</p></li>
<li><p>The size of AUP3 projects may increase greatly in size even for a small
project. If you encounter this, please file a bug report at
<a class="reference external" href="https://codeberg.org/tenacityteam/tenacity/issues">https://codeberg.org/tenacityteam/tenacity/issues</a>. Such behavior is likely
a bug.</p></li>
</ul>
</div>
</section>
<section id="os">
<h3>OS<a class="headerlink" href="#os" title="Link to this heading">¶</a></h3>
<ul class="simple">
<li><p><strong>Windows</strong>: Windows 7 or higher</p></li>
<li><p><strong>macOS</strong>: If building from source, macOS 10.15 is the minimum supported
version.</p></li>
<li><p><strong>Linux and other OSes</strong>: If there’s a Tenacity package available for your
platform, it will have everything taken care of for you. Optionally install
FFmpeg for wider import/export support. Otherwise, you should have the
following dependencies available:</p>
<ul>
<li><p>wxWidgets 3.1.3 or later <strong>built against GTK 3</strong>. We recommend at least
3.1.5 for HiDPI support. The latest version is preferred.</p></li>
<li><p>For MIDI support: PortMidi and PortSMF (see notes for more information).</p></li>
<li><p>For high-quality stretching: libsbsms 2.1.0 or later.</p></li>
<li><p>All other dependencies: the latest versions available are recommended.</p></li>
</ul>
</li>
</ul>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p><strong>On Linux and other OSes</strong>:</p>
<ul class="simple">
<li><p>We only support wxGTK with GTK 3 officially; GTK 2 is not supported and
any GTK 2 specific bugs won’t be fixed. The only other wxWidgets port
we support is wxQt, although that requires patches and is meant for
advanced users only.</p></li>
<li><p>PortMidi, PortSMF, and libsbsms are not available on all distros, or (for
libsbsms) your distro might contain the wrong version. For PortSMF, the
packages might be very old (at least on Debian). Installing the Flatpak
takes care of all these issues.</p></li>
</ul>
</div>
<section id="sound-devices">
<h4>Sound Devices<a class="headerlink" href="#sound-devices" title="Link to this heading">¶</a></h4>
<p>Most sound devices should work out of the box with Tenacity. If Tenacity is not
detecting our set of speakers, microphone, or other audio input or output
device, try the following:</p>
<ul class="simple">
<li><p>Try a different host. Click on first dropdown menu of the device toolbar and
select a different host. Then check to see if the device is there by checking
either the input or output devices.</p></li>
<li><p>Make sure you have the proper drivers installed. Visit the manufacturer’s
website for more info.</p></li>
<li><p>Test with another application. Does it show up in your system’s settings? Does
a media player or other application see the device? If so, this might be a
bug or a limitation of Tenacity.</p></li>
</ul>
</section>
</section>
</section>
<section id="installation-on-windows">
<h2>Installation on Windows<a class="headerlink" href="#installation-on-windows" title="Link to this heading">¶</a></h2>
<p>To install on Windows, simply download one of the installers and run them.
Follow the on-screen instructions to install Tenacity. This also works for
upgrading Tenacity from prior versions.</p>
</section>
<section id="installation-on-linux">
<h2>Installation on Linux<a class="headerlink" href="#installation-on-linux" title="Link to this heading">¶</a></h2>
<section id="flatpak">
<h3>Flatpak<a class="headerlink" href="#flatpak" title="Link to this heading">¶</a></h3>
<p>Tenacity is available on Flathub. First, add it if you haven’t already:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>$<span class="w"> </span>flatpak<span class="w"> </span>remote-add<span class="w"> </span>--if-not-exists<span class="w"> </span>flathub<span class="w"> </span>https://dl.flathub.org/repo/flathub.flatpakrepo
</pre></div>
</div>
<p>To install Tenacity, run the following:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>$<span class="w"> </span>flatpak<span class="w"> </span>install<span class="w"> </span>org.tenacityaudio.Tenacity
</pre></div>
</div>
<p>Alternatively, you may use a GUI frontend like GNOME Software or KDE Discover.
Once Flathub is enabled, search “tenacity”, and it should be the first result.
If you don’t see the result, you might need to enable Flathub first.</p>
</section>
<section id="appimage">
<h3>AppImage<a class="headerlink" href="#appimage" title="Link to this heading">¶</a></h3>
<p>Some versions of Tenacity are available in the Appimage format. You may choose
to use these AppImages for convenience, but they are provided as-is and will
not receive support for any AppImage-specific issues. Any issues with Tenacity
itself will still be supported.</p>
<p>There is no need to install an AppImage. However, the closest way to
“installing” the Tenacity AppImage on your system is by placing it in a
directory (e.g., <code class="docutils literal notranslate"><span class="pre">/opt</span></code>) and adding that directory to your <code class="docutils literal notranslate"><span class="pre">$PATH</span></code>. For
example:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>$<span class="w"> </span>cp<span class="w"> </span>tenacity.AppImage<span class="w"> </span>/opt/bin/tenacity<span class="w"> </span><span class="c1"># Copy the Tenacity AppImage to /opt/bin</span>
$<span class="w"> </span>nano<span class="w"> </span>~/.bashrc<span class="w">                         </span><span class="c1"># Edit your .bashrc to add /opt/bin to your $PATH</span>
$<span class="w"> </span>tenacity<span class="w">                               </span><span class="c1"># Run Tenacity</span>
</pre></div>
</div>
<p>(Note that the following does NOT add a <code class="docutils literal notranslate"><span class="pre">.desktop</span></code> entry; Tenacity will not
show up in desktop environments).</p>
</section>
</section>
<section id="advanced-building">
<h2>Advanced: Building<a class="headerlink" href="#advanced-building" title="Link to this heading">¶</a></h2>
<p>Alternatively, you can build Tenacity yourself on any supported platform.
Build instructions can be found <a class="reference external" href="https://codeberg.org/tenacityteam/tenacity/src/main/BUILDING.md">here</a>.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>Building is meant for more advanced users and requires good CPU, RAM, and
storage resources.</p>
</div>
<section id="building-an-appimage">
<h3>Building an AppImage<a class="headerlink" href="#building-an-appimage" title="Link to this heading">¶</a></h3>
<p>It is also possible to build an AppImage. After building Tenacity, set the
environment variable <code class="docutils literal notranslate"><span class="pre">VCPKG_LIB_PATH</span></code> to the path vcpkg installed libraries to
(usually in the root of the build folder under
<code class="docutils literal notranslate"><span class="pre">vcpkg_installed/&lt;triplet&gt;/lib</span></code>). If you did not use vcpkg while building
Tenacity, set it to an empty string (i.e., <code class="docutils literal notranslate"><span class="pre">export</span> <span class="pre">VCPKG_LIB_PATH=&quot;&quot;</span></code>). Then
run <code class="docutils literal notranslate"><span class="pre">cpack</span></code> at the root of the build folder. If everything succeeds, the
AppImage will be under <code class="docutils literal notranslate"><span class="pre">package/</span></code> as
<code class="docutils literal notranslate"><span class="pre">tenacity-linux-&lt;tag&gt;-XXXXX-gXXXXXX.AppImage</span></code>.</p>
</section>
</section>
<section id="migrating-from-previous-versions">
<span id="migration-notes"></span><h2>Migrating from Previous Versions<a class="headerlink" href="#migrating-from-previous-versions" title="Link to this heading">¶</a></h2>
<section id="upgrading-from-betas">
<h3>Upgrading From Betas<a class="headerlink" href="#upgrading-from-betas" title="Link to this heading">¶</a></h3>
<p>Upgrading from beta releases is usually not supported, but due to the special
circumstances regarding the 1.3 betas, upgrading from those versions to stable
releases will be supported for now.</p>
</section>
<section id="builtin-effects-are-disabled">
<h3>Builtin Effects Are Disabled<a class="headerlink" href="#builtin-effects-are-disabled" title="Link to this heading">¶</a></h3>
<p>It is a known issue that upgrading from any of the 1.3 betas to a stable
version causes you to loose access to all plugins. This is because some
information about the builtin effects was changed, and due to the way
Tenacity’s plugin manager works, it fails “loading” builtin effects.</p>
<p>The solution is quite simple: delete the old plugin registry
(pluginregistry.cfg) from your settings folder. If you are on Windows, delete
<code class="docutils literal notranslate"><span class="pre">C:\Users&lt;username&gt;\AppData\Roaming\Tenacity\pluginregistry.cfg</span></code>. If you are
on Linux, there are two different files you need to delete depending on if you
use the Flatpak or not:</p>
<ul class="simple">
<li><p>Delete <code class="docutils literal notranslate"><span class="pre">~/.var/app/org.tenacityaudio.Tenacity/config/pluginregistry.cfg</span></code> if
<strong>DO</strong> use Flatpak.</p></li>
<li><p>Delete <code class="docutils literal notranslate"><span class="pre">~/.config/tenacity/pluginregistry.cfg</span></code> if you <strong>DON’T</strong> use Flatpak.</p></li>
</ul>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>For your convenience, Tenacity shows the location of your settings folder
under <em>Help &gt; About Tenacity… &gt; Build Information</em>. If the location of
your settings folder differs from the ones mentioned above, use this
location.</p>
</div>
</section>
</section>
</section>


      </div>
      <div class="bottomnav" role="navigation" aria-label="Bottom">
      
        <p>
        «&#160;&#160;<a href="Introduction_and_Motivation.html">Introduction</a>
        &#160;&#160;::&#160;&#160;
        <a class="uplink" href="../index.html">Contents</a>
        &#160;&#160;::&#160;&#160;
        <a href="Basics.html">Basics</a>&#160;&#160;»
        </p>

      </div>

    <div class="footer" role="contentinfo">
    &#169; Copyright 2022-2024 Tenacity Community. Licensed under CC-BY 4.0.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 8.1.3.
    </div>
  </body>
</html>