(ffmpeg)=

# FFmpeg in Tenacity

FFmpeg allows you to encode and decode many different video and audio formats.
Using FFmpeg with Tenacity allows you to handle numerous different audio
formats and codecs otherwise not possible in Tenacity due to various (legal)
reasons.

Tenacity 1.3 officially supports FFmpeg 3.x and above, including up to FFmpeg
5.x. However, due to licensing restrictions on the various formats FFmpeg
supports, Tenacity must load FFmpeg instead.

## General Compatibility Notes

- **Do not mix 32-bit libraries with a 64-bit copy of Tenacity and vice
  versa.** It will not work and is not supported.

## FFmpeg on Windows

We provide our own FFmpeg distribution with each stable release of Tenacity.
The version we provide will usually be the latest stable version at the time
the release is made.

FFmpeg downloads are provided alongside regular Tenacity downloads over at
[our releases page](https://codeberg.org/tenacityteam/tenacity/releases). The
FFmpeg downloads begin with either `tenacity-ffmpeg` or `ffmpeg-tenacity` You
also do not need to install them.

Our builds are currently released under GPLv3, per our configuration.
Additionally, we only provide libavformat along with its dependencies,
libavcodec and libavutil. There is also no need to install these libraries.

## FFmpeg on Linux

Tenacity should automatically detect and load your distro's FFmpeg libraries
if installed. If FFmpeg is not installed, you can install them through your
distro's package manager. Please consult your distro's documentation for
assistance.

## Using FFmpeg Libraries

On Windows, follow the steps below to enable FFmpeg:

1. Download a copy of FFmpeg. See our releases page to grab a copy.
2. Extract the libraries to a suitable folder.
3. In Tenacity, go to  _Preferences > Libraries > FFmpeg Import/Export Library_
   and click the _Locate..._ button.
4. If FFmpeg was automatically set up before, you will be asked if you want to
   search for new libraries. Click _Yes_ if you are updating FFmpeg.
5. A dialog will pop up prompting you to locate FFmpeg. Click _Browse..._ and
   open `avformat.dll` (the name may be slightly different but it doesn't
   matter).
6. If all went well, you should see a version string like this:
   `F(60.3.100),C(60.3.100),U(58.2.100)`.

On Linux, you should install your distro's FFmpeg package as that's the
preferred method. If, for some reason, you want to use custom FFmpeg libraries,
you can follow the steps above with your own FFmpeg libraries, searching for
`libavformat.so` instead (again, the name may be slightly different, but it
doesn't matter).

If you already have valid libraries, a dialog will ask you if you still want to
locate FFmpeg libraries manually. Click 'Yes' to continue locating them
manually.
